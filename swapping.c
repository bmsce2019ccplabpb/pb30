#include<stdio.h>
void swap(int*a,int*b)
{
int temp;
temp = *a;
*a=*b;
*b=temp;
}
int main()
{
int n1,n2;
printf("\nEnter two numbers : ");
scanf("%d %d",&n1,&n2);
printf("\nThe value of n1&n2 before call : %d %d",n1,n2);
swap(&n1,&n2);
printf("\nThe value of n1 &n2 after call or swapping : %d %d",n1,n2);
return 0;
}
