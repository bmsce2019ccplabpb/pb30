#include<stdio.h>

int main()
{
    int i,j,m,n,matrix_1[10][10],matrix_2[10][10],diff_matrix[10][10];
    printf("Enter the number of rows and columns\n");
    scanf("%d%d",&m,&n);
    printf("Enter the first matrix\n");
    for(i=0; i<m; i++)
    {
        for(j=0; j<n; j++)
        {
            scanf("%d",&matrix_1[i][j]);
        }
    }
    printf("Enter the elements of the second matrix\n");
    for(i=0; i<m; i++)
    {
        for(j=0; j<n; j++)
        {
            scanf("%d",&matrix_2[i][j]);
        }
    }
    printf("The difference of the two matrices is :\n");
    for(i=0; i<m; i++)
    {
        for(j=0; j<n; j++)
        {
            diff_matrix[i][j]=matrix_1[i][j]-matrix_2[i][j];
            printf("%d",diff_matrix[i][j]);
        }
    }
    printf("\n");
    return 0;
}